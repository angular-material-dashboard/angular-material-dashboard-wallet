/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardWallet', [ //
    'ngMaterialDashboard',//
    'ngMaterialDashboardBank',//
    'seen-bank'
]);

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardWallet')
/**
 * 
 */
.config(function ($routeProvider) {
    $routeProvider //
    .when('/wallets', {
        controller : 'AmdBankWalletsCtrl',
        controllerAs: 'ctrl',
        templateUrl : 'views/amd-bank-wallets.html',
        navigate : true,
        protect : function ($rootScope) {
            return !$rootScope.app.user.tenant_owner;
        },
        name : 'Wallets',
        groups : [ 'bank' ],
        icon : 'account_balance_wallet',
        hidden : '!app.user.tenant_owner'
    }) //
    .when('/wallets/new', {
        controller : 'AmdBankWalletNewCtrl',
        templateUrl : 'views/amd-bank-wallet-new.html',
        controllerAs: 'ctrl',
        navigate : false,
        name : 'New wallet',
        groups : [ 'bank' ],
        icon : 'add',
        protect : true
    }) //
    .when('/my-wallets', {
        controller : 'AmdBankWalletsCtrl',
        templateUrl : 'views/amd-bank-my-wallets.html',
        controllerAs: 'ctrl',
        navigate : true,
        protect : true,
        name : 'My wallets',
        groups : [ 'bank' ],
        icon : 'account_balance_wallet'
    })//
    .when('/wallet-to-wallet', {
        templateUrl : 'views/amd-bank-wallet-to-wallet.html',
        controller : 'AmdBankWalletToWalletCtrl',
        controllerAs: 'ctrl',
        navigate : true,
        name : 'Wallet to wallet',
        icon : 'swap_horiz',
        groups : [ 'bank' ],
        protect : true
    }) //
    .when('/wallets/charge', {
        templateUrl : 'views/amd-bank-wallet-charge.html',
        controller : 'AmdBankWalletChargeCtrl',
        controllerAs: 'ctrl',
        navigate : true,
        name : 'Charge wallet',
        icon : 'battery_charging_80',
        groups : [ 'bank' ],
        protect : true
    })//
    .when('/wallets/:walletId/payments/:paymentId', {
        templateUrl : 'views/amd-bank-wallet-payment.html',
        controller : 'AmdBankWalletPaymentCtrl',
        controllerAs: 'ctrl',
        protect : true
    })//
    .when('/wallets/:walletId', {
        templateUrl : 'views/amd-bank-wallet.html',
        controller : 'AmdBankWalletCtrl',
        controllerAs: 'ctrl',
        protect : true
    }); 
});

'use strict';

angular.module('ngMaterialDashboardWallet')//

/**
 * @ngdoc Controller
 * @name AmdBankWalletChargeCtrl
 * @description # Charge a wallet
 * 
 */
.controller('AmdBankWalletChargeCtrl', function ($bank, $rootScope, QueryParameter, $window, $routeParams, $translate) {

    this.queryParameter = new QueryParameter();
    this.wallet = {};

    this.loadUserWallets = function () {
        if (this.loading) {
            return;
        }
        if (this.wallets) {
            return this.wallets;
        }
        this.loading = true;
        this.queryParameter.addFilter('owner_id', $rootScope.app.user.current.id);
        this.queryParameter.addFilter('deleted', false);
        var ctrl = this;
        return $bank.getWallets(this.queryParameter)//
        .then(function (response) {
            ctrl.loading = false;
            ctrl.wallets = response.items;
            ctrl.findInitialWallet();
            return ctrl.wallets;
        });
    };

    this.findInitialWallet = function () {
        if (!$routeParams.walletId) {
            return;
        }
        for (var i = 0; i < this.wallets.length; i++) {
            if (JSON.stringify(this.wallets[i].id) === $routeParams.walletId) {
                this.wallet = this.wallets[i];
                this.setCurrency();
                return;
            }
        }
    };

    /*
     * Find suitable currency for the wallet
     */
    this.setCurrency = function () {
        this.currency = this.wallet.currency;
        this.setCallbackUrl();
    };

    /*
     * Set callbackUrl which is needed in mb-pay directive
     */
    this.setCallbackUrl = function () {
        this.callbackUrl = '/wallets/' + this.wallet.id + '/payments/{{id}}';
    };

    this.pay = function (data) {
        data.title = $translate.instant('Charge wallet');
        data.amount = this.amount;
        data.description = $translate.instant('Charge ' + this.wallet.title);
        // create receipt and send to bank receipt page.
        return this.wallet.putPayment(data)
        .then(function (payment) {
            return $bank.getReceipt(payment.receipt_id)
            .then(function (receipt) {
                return receipt;
            });
        });
    };

    this.cancel = function () {
        $window.history.back();
    };
});

'use strict';

angular.module('ngMaterialDashboardWallet')

/**
 * @ngdoc controller
 * @name AmdBankWalletNewCtrl
 * @description Create new wallet
 */
.controller('AmdBankWalletNewCtrl', function($bank, $navigator, $translate, $http) {
    
	this.creatingWallet = false;

	this.cancel = function () {
		$navigator.openPage('/wallets');
	};
	
	this.currencies = {};
	var ctrl = this;
	$http({
	    method: 'GET',
	    url: 'https://openexchangerates.org/api/currencies.json'
	}).then(function (response) {
	    var currencies = response.data;
	    // Add Toman to the list
	    currencies.IRT = 'Iranian Toman';
	    ctrl.currencies = currencies;
	});

	this.add = function (wallet) {
	    if (this.creatingWallet) {
		return;
	    }
	    this.creatingWallet = true;
	    var ctrl = this;
	    $bank.putWallet(wallet)
		    .then(function(wallet) {
			ctrl.creatingWallet = false;
			$navigator.openPage('/wallets');
		    }, function() {
			    ctrl.creatingWallet = false;
			    alert($translate.instant('Fail to create new wallet'));
		    });
	};
});

'use strict';

angular.module('ngMaterialDashboardWallet')

	/**
	 * @ngdoc controller
	 * @name AmdBankWalletPaymentCtrl
	 * @description Manage payments of a wallet
	 * 
	 */
	.controller('AmdBankWalletPaymentCtrl', function ($bank, $navigator, $routeParams, $translate) {

	    this.walletId = $routeParams.walletId;
	    this.paymentId = $routeParams.paymentId;

	    this.paymentChecked = false;

	    /*
	     * load wallet
	     */
	    this.load = function () {
		if (this.loading) {
		    return;
		}
		this.loading = true;
		var ctrl = this;
		return $bank.getWallet(this.walletId)//
			.then(function (wallet) {
			    ctrl.wallet = wallet;
			    ctrl.loadPayment();
			}, function () {
			    alert($translate.instant('Failed to load wallet'));
			})//
			.finally(function () {
			    ctrl.loading = false;
			});
	    };

	    /*
	     * load payment
	     */
	    this.loadPayment = function () {
		if (this.loadingPayment) {
		    return;
		}
		this.loadingPayment = true;
		var ctrl = this;
		//TODO: masood,2019: Check for graphql
		return this.wallet.getPayment(this.paymentId)//
			.then(function (payment) {
			    ctrl.paymentChecked = true;
			    ctrl.payment = payment;
			    //TODO: masood,2019: Get with graphql
			    return $bank.getReceipt(payment.receipt_id);
			}, function () {
			    alert($translate.instant('Failed to load payment'));
			})//
			.then(function (receipt) {
			    ctrl.receipt = receipt;
			})//
			.finally(function () {
			    ctrl.loadingPayment = false;
			});
	    };

	    this.pay = function () {
		$navigator.openPage('bank/receipts/' + this.receipt.id);
	    };

	    this.load();
	});

'use strict';

angular.module('ngMaterialDashboardWallet')

/**
 * @ngdoc controller
 * @name AmdBankWalletPaymentsCtrl
 * @description Load payments of a wallet
 * 
 */
.controller('AmdBankWalletPaymentsCtrl', function ($scope, $routeParams, $q, $translate, $bank, $navigator, $controller) {

    // Extends with ItemsController
    angular.extend(this, $controller('MbSeenAbstractCollectionCtrl', {
        $scope: $scope
    }));

    this.walletId = $routeParams.walletId;

    /*
     * Load wallet
     */
    this.loadWallet = function () {
        if(this.wallet){
            return $q.when(this.wallet);
        }
        if (this.loadingWallet) {
            return this.loadingWallet;
        }
        var ctrl = this;
        this.loadingWallet = $bank.getWallet(this.walletId)//
        .then(function (wallet) {
            ctrl.wallet = wallet;
        }, function () {
            alert($translate.instant('Failed to load wallet'));
        })//
        .finally(function () {
            delete ctrl.loadingWallet;
        });
        return this.loadingWallet;
    };

    /*
     * Overried the function
     */
    this.getModelSchema = function () {
        var ctrl = this;
        return this.loadWallet()//
        .then(function () {
            return ctrl.wallet.paymentSchema();
        });
    };

    // get wallets
    this.getModels = function (parameterQuery) {
        var ctrl = this;
        return this.loadWallet()//
        .then(function () {
            return ctrl.wallet.getPayments(parameterQuery);
        });
    };

    this.init({
        eventType: '/bank/wallets'
    });
});

'use strict';

angular.module('ngMaterialDashboardWallet')//

	/**
	 * @ngdoc Controller
	 * @name AmdBankWalletTransferCtrl
	 * @description # Wallet to wallet transfer
	 * 
	 */
	.controller('AmdBankWalletToWalletCtrl', function ($bank, $rootScope, QueryParameter, $window, $routeParams, $translate) {

	    this.queryParameter = new QueryParameter();
	    this.sourceWallet = {};


	    this.loadUserWallets = function () {
		if (this.loading) {
		    return;
		}
		if (this.wallets) {
		    return this.wallets;
		}
		this.loading = true;
		this.queryParameter.addFilter('owner_id', $rootScope.app.user.current.id);
		this.queryParameter.addFilter('deleted', false);
		var ctrl = this;
		return $bank.getWallets(this.queryParameter)//
			.then(function (response) {
			    ctrl.wallets = response.items;
			    ctrl.findInitialWallet();
			    return ctrl.wallets;
			});
	    };

	    this.findInitialWallet = function () {
		if (!$routeParams.fromId) {
		    return;
		}
		for (var i = 0; i < this.wallets.length; i++) {
		    if (JSON.stringify(this.wallets[i].id) === $routeParams.fromId) {
			this.sourceWallet = this.wallets[i];
			return;
		    }
		}
	    };

	    this.transfer = function () {
		if (this.transfering) {
		    return;
		}
		if (this.destWalletId === this.sourceWallet.id) {
		    this.message = 'Two wallets are equal. Transfering is impossible';
		    return;
		}
		this.transfering = true;
		var ctrl = this;
		return this.sourceWallet.putTransfer({
			to_wallet_id: this.destWalletId,
			amount: this.amount,
			description: this.description
		    })//
		    .then(function (transfer) {
			$window.history.back();
		    }, function (error) {
			alert($translate.instant('Failed to transfer'));
		    })
		    .finally(function () {
			ctrl.transfering = false;
		    });
	    };

	    this.cancel = function () {
		$window.history.back();
	    };

	    this.loadUserWallets();

	});

'use strict';

angular.module('ngMaterialDashboardWallet')

/**
 * @ngdoc controller
 * @name AmdBankWalletTransfersCtrl
 * @description Load transfers of a wallet
 * 
 */
.controller('AmdBankWalletTransfersCtrl', function ($scope, $routeParams, $q, $translate, $bank, $navigator, $controller) {

    // Extends with ItemsController
    angular.extend(this, $controller('MbSeenAbstractCollectionCtrl', {
        $scope: $scope
    }));

    this.walletId = $routeParams.walletId;

    /*
     * Load wallet
     */
    this.loadWallet = function () {
        if(this.wallet){
            return $q.when(this.wallet);
        }
        if (this.loadingWallet) {
            return this.loadingWallet;
        }
        var ctrl = this;
        this.loadingWallet = $bank.getWallet(this.walletId)//
        .then(function (wallet) {
            ctrl.wallet = wallet;
        }, function () {
            alert($translate.instant('Failed to load wallet'));
        })//
        .finally(function () {
            delete ctrl.loadingWallet;
        });
        return this.loadingWallet;
    };

    /*
     * Overried the function
     */
    this.getModelSchema = function () {
        var ctrl = this;
        return this.loadWallet()//
        .then(function () {
            return ctrl.wallet.transferSchema();
        });
    };

    // get wallets
    this.getModels = function (parameterQuery) {
        var ctrl = this;
        return this.loadWallet()//
        .then(function () {
            return ctrl.wallet.getTransfers(parameterQuery);
        });
    };

    this.init({
        eventType: '/bank/transfers'
    });
});

'use strict';

angular.module('ngMaterialDashboardWallet')

	/**
	 * @ngdoc controller
	 * @name AmdBankWalletCtrl
	 * @description Manage a wallet
	 * 
	 */
	.controller('AmdBankWalletCtrl', function ($bank, $location, $routeParams, $translate) {

	    this.walletId = $routeParams.walletId;

	    this.load = function () {
		if (this.loading) {
		    return;
		}
		this.loading = true;
		var ctrl = this;
		return $bank.getWallet(this.walletId)//
			.then(function (wallet) {
			    ctrl.wallet = wallet;
			}, function () {
			    alert($translate.instant('Failed to load wallet'));
			})//
			.finally(function () {
			    ctrl.loading = false;
			});
	    }; 
	    
	    this.remove = function () {
		var ctrl = this;
		confirm($translate.instant('The wallet will be deleted.'))//
			.then(function () {
			    return ctrl.wallet.delete();//
			})//
			.then(function () {
			    $location.path('/wallets');
			}, function () {
			    alert($translate.instant('Failed to delete wallet'));
			});
	    };

	    this.update = function () {
		if (this.saving) {
		    return;
		}
		this.saving = true;
		var ctrl = this;
		return this.wallet.update()//
			.then(function (wallet) {
			    ctrl.wallet = wallet;
			}, function () {
			    alert($translate.instant('Failed to save wallet'));
			})
			.finally(function () {
			    ctrl.saving = false;
			});
	    };

	    this.load();
	});

'use strict';

angular.module('ngMaterialDashboardWallet')

	/**
	 * @ngdoc controller
	 * @name AmdBankWalletsCtrl
	 * @description Manages wallets
	 * 
	 */
	.controller('AmdBankWalletsCtrl', function ($scope, $bank, $navigator, $controller) {

	    // Extends with ItemsController
	    angular.extend(this, $controller('MbSeenAbstractCollectionCtrl', {
		$scope: $scope
	    }));

	    /*
	     * Overried the function
	     */
	    this.getModelSchema = function () {
		return $bank.walletSchema();
	    };

	    // get wallets
	    this.getModels = function (parameterQuery) {
		return $bank.getWallets(parameterQuery);
	    };

	    // get a wallet
	    this.getModel = function (id) {
		return $bank.getWallet(id);
	    };

	    // delete wallet
	    this.deleteModel = function (item) {
		return item.delete();
	    };

	    // create new 
	    this.createWallet = function () {
		$navigator.openPage('/wallets/new');
	    };

	    var ctrl = this;
	    this.addActions([{
		    title: 'New wallet',
		    icon: 'add',
		    action: function () {
			ctrl.createWallet();
		    }
		}]);

	    this.init({
		eventType: '/bank/wallets'
	    });
	});

angular.module('ngMaterialDashboardWallet').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/amd-bank-my-wallets.html',
    "<div ng-init=\"ctrl.queryParameter.setFilter('owner_id', app.user.current.id)\" layout=column flex> <mb-pagination-bar mb-model=ctrl.queryParameter mb-reload=ctrl.reload() mb-sort-keys=ctrl.getProperties() mb-more-actions=ctrl.actions> </mb-pagination-bar> <md-content mb-preloading=\"ctrl.state==='busy'\" mb-infinate-scroll=ctrl.loadNextPage() layout=column flex>  <md-list flex> <md-list-item ng-repeat=\"wallet in ctrl.items track by wallet.id\" ng-if=!wallet.deleted ng-href=wallets/{{::wallet.id}} class=md-3-line> <wb-icon>account_balance_wallet</wb-icon> <div class=md-list-item-text layout=column> <h3>{{::wallet.title}}</h3> <h4><span translate=\"\">Balance</span>: {{::wallet.total_deposit - wallet.total_withdraw|| 0}} ({{::wallet.currency}})</h4> <p>{{::wallet.description}}</p> </div> <md-button class=md-icon-button aria-label=\"Charge wallet\" ng-href=\"/wallets/charge?walletId={{::wallet.id}}\"> <wb-icon>battery_charging_80</wb-icon> <md-tooltip md-direction=bottom><span translate=\"\">Charge wallet</span></md-tooltip> </md-button> <md-button class=md-icon-button aria-label=\"Wallet to wallet\" ng-href=\"/wallet-to-wallet?fromId={{::wallet.id}}\"> <wb-icon>swap_horiz</wb-icon> <md-tooltip md-direction=bottom><span translate=\"\">Wallet to wallet</span></md-tooltip> </md-button> <md-button class=md-icon-button aria-label=\"Delete wallet\" ng-click=ctrl.deleteItem(wallet)> <wb-icon>delete</wb-icon> <md-tooltip><span translate=\"\">Delete wallet</span></md-tooltip> </md-button> </md-list-item> </md-list> </md-content> <div layout=column layout-align=\"center center\" ng-if=\"ctrl.state === 'ideal' && (!ctrl.items || ctrl.items.length === 0)\"> <h2 translate=\"\">Nothing found</h2> </div> </div>"
  );


  $templateCache.put('views/amd-bank-wallet-charge.html',
    "<md-content layout-padding flex> <mb-titled-block mb-progress=ctrl.charging mb-title=\"{{'Charge wallet'| translate}}\" layout=column> <form name=contentForm ng-action=ctrl.charge() mb-preloading=ctrl.charging layout-margin layout=column flex> <div layout=column layout-padding> <md-input-container> <label translate=\"\">Choose wallet</label> <md-select ng-model=ctrl.wallet ng-change=ctrl.setCurrency() md-on-open=ctrl.loadUserWallets() required> <md-option ng-value=wallet ng-repeat=\"wallet in ctrl.wallets\"> {{::wallet.title}} </md-option> </md-select> </md-input-container> <md-input-container> <label translate>Amount</label> <input required type=number name=amount ng-model=ctrl.amount min=1> <div ng-messages=contentForm.amount.$error ng-if=contentForm.amount.$touched> <div ng-message=required><span translate>Amount should be positive.</span></div> <div ng-message=min><span translate>Minimum supported amount is reached.</span></div> </div> </md-input-container>  <div layout-padding layout-margin> <mb-pay mb-pay=ctrl.pay($data) mb-callback-url={{ctrl.callbackUrl}} mb-discount-enable=true ng-model=ctrl.currency> </mb-pay> </div> <div layout-gt-xs=row layout=column layout-margin> <span flex></span> <md-button class=\"md-raised md-accent\" ng-click=ctrl.cancel() aria-label=cancel> <span translate=\"\">Cancel</span> </md-button> </div> <div ng-if=ctrl.message> <p style=\"color: red\"><span translate>{{ctrl.message}}</span></p> </div> </div> </form> </mb-titled-block> </md-content>"
  );


  $templateCache.put('views/amd-bank-wallet-new.html',
    "<md-content class=md-padding layout-padding flex> <form name=contentForm ng-action=ctrl.add(wallet) mb-preloading=ctrl.creatingWallet layout-margin layout=column flex> <md-input-container> <label translate=\"\">Title</label> <input name=title ng-model=wallet.title required> <div ng-messages=contentForm.title.$error> <div ng-message=required><span translate=\"\">This field is required.</span></div> </div> </md-input-container> <md-select placeholder=\"{{'Currency'| translate}}\" ng-model=wallet.currency required> <md-option ng-value=key ng-repeat=\"(key,val) in ctrl.currencies track by $index\"> {{val}} </md-option> </md-select> <span flex=5></span> <md-input-container> <label translate=\"\">Description</label> <input ng-model=wallet.description> </md-input-container> <div layout=row> <span flex></span> <md-button class=\"md-raised md-accent\" ng-click=ctrl.cancel() aria-label=close> <span translate>Cancel</span> </md-button> <md-button class=\"md-raised md-primary\" type=submit ng-disabled=contentForm.$invalid ng-click=ctrl.add(wallet) aria-label=\"add wallet\"> <span translate>Add</span> </md-button> </div> </form> </md-content>"
  );


  $templateCache.put('views/amd-bank-wallet-payment.html',
    "<md-content class=md-padding layout-padding flex> <mb-titled-block mb-progress=\"ctrl.loading || ctrl.loadingPayment\" mb-title=\"{{'Payment'| translate}}\" layout=column> <div layout=row layout-xs=column layout-align-xs=\"center center\" layout-padding> <table> <tr> <td translate=\"\">ID </td> <td>: {{ctrl.payment.id}}</td> </tr> <tr> <td translate=\"\">Amount </td> <td>: {{ctrl.payment.amount}}</td> </tr> <tr> <td translate=\"\">Receipt Id </td> <td>: {{ctrl.payment.receipt_id}}</td> </tr> <tr> <td translate=\"\">Description </td> <td>: {{ctrl.payment.description}}</td> </tr> <tr> <td translate=\"\">Creation Date </td> <td>: {{ctrl.payment.creation_dtime| mbDate:'jYYYY-jMM-jDD hh:mm:ss'}}</td> </tr> <tr ng-if=ctrl.payment.modif_dtime> <td translate=\"\">Last Modified Date </td> <td>: {{ctrl.payment.modif_dtime| mbDate:'jYYYY-jMM-jDD hh:mm:ss'}}</td> </tr> <tr> <td translate=\"\">Status </td> <td ng-if=\"!ctrl.loading && !ctrl.loadingPayment\"> : <span style=\"color: red\" translate=\"\"> {{(ctrl.receipt.payRef ? 'Is payed' : 'Is not payed')}} </span> </td> </tr> </table> </div>  <div ng-if=\"!ctrl.loading && !ctrl.loadingPayment && ctrl.paymentChecked && !ctrl.receipt.payRef\" layout=row> <span flex></span> <md-button class=\"md-raised md-accent\" ng-click=ctrl.pay()> <span translate>Pay</span> </md-button> </div> </mb-titled-block> </md-content>"
  );


  $templateCache.put('views/amd-bank-wallet-to-wallet.html',
    "<md-content layout-padding flex> <mb-titled-block mb-progress=ctrl.transfering mb-title=\"{{'Wallet to wallet transfering'| translate}}\" layout=column> <form name=contentForm ng-action=ctrl.transfer() mb-preloading=ctrl.transfering layout-margin layout=column flex> <div layout=column layout-padding> <md-input-container> <label translate=\"\">Source wallet</label> <md-select ng-model=ctrl.sourceWallet required> <md-option ng-value=wallet ng-repeat=\"wallet in ctrl.wallets\"> {{::wallet.title}} </md-option> </md-select> </md-input-container> <md-input-container> <label translate>Destination wallet id</label> <input required name=dest type=number ng-model=ctrl.destWalletId min=1> <div ng-messages=contentForm.dest.$error ng-if=contentForm.dest.$touched> <div ng-message=required>Source wallet id is required and a positive number.</div> <div ng-message=min>Minimum supported amount is reached.</div> </div> </md-input-container> <md-input-container> <label translate>Amount</label> <input required type=number name=amount ng-model=ctrl.amount min=1 max=\"{{ctrl.sourceWallet.total_deposit - ctrl.sourceWallet.total_withdraw}}\"> <div ng-messages=contentForm.amount.$error ng-if=contentForm.amount.$touched> <div ng-message=required><span translate>Amount should be positive.</span></div> <div ng-message=min><span translate>Minimum supported amount is reached.</span></div> <div ng-message=max><span translate>Wallet balance is insufficient.</span></div> </div> </md-input-container> <md-input-container> <label translate>Description</label> <input ng-model=ctrl.description> </md-input-container> <div layout-gt-xs=row layout=column> <span flex></span> <md-button class=\"md-raised md-accent\" ng-click=ctrl.cancel() aria-label=cancel> <span translate=\"\">Cancel</span> </md-button> <md-button class=\"md-raised md-primary\" type=submit ng-disabled=contentForm.$invalid ng-click=ctrl.transfer() aria-label=transfer> <span translate>Transfer</span> </md-button> </div> <div ng-if=ctrl.message> <p style=\"color: red\"><span translate>{{ctrl.message}}</span></p> </div> </div> </form> </mb-titled-block> </md-content>"
  );


  $templateCache.put('views/amd-bank-wallet.html',
    "<md-content class=md-padding layout-padding flex> <mb-titled-block ng-show=!ctrl.edit mb-progress=\"ctrl.loading || ctrl.saving\" mb-title=\"{{'Wallet'| translate}}\" layout=column> <div layout=row layout-xs=column layout-align-xs=\"center center\" layout-padding> <table> <tr> <td translate=\"\">ID </td> <td>: {{ctrl.wallet.id}}</td> </tr> <tr> <td translate=\"\">Title </td> <td>: {{ctrl.wallet.title}}</td> </tr> <tr> <td translate=\"\">Currency </td> <td>: {{ctrl.wallet.currency}}</td> </tr> <tr> <td translate=\"\">Total deposit </td> <td>: {{ctrl.wallet.total_deposit|| 0}}</td> </tr> <tr> <td translate=\"\">Total withdrawal </td> <td>: {{ctrl.wallet.total_withdraw|| 0}}</td> </tr> <tr> <td translate=\"\">Balance </td> <td>: {{ctrl.wallet.total_deposit - ctrl.wallet.total_withdraw|| 0}}</td> </tr> <tr> <td translate=\"\">Description </td> <td>: {{ctrl.wallet.description}}</td> </tr> <tr> <td translate=\"\">Creation Date </td> <td>: {{ctrl.wallet.creation_dtime| mbDate:'jYYYY-jMM-jDD hh:mm:ss'}}</td> </tr> <tr> <td translate=\"\">Last Modified Date </td> <td>: {{ctrl.wallet.modif_dtime| mbDate:'jYYYY-jMM-jDD hh:mm:ss'}}</td> </tr> </table> </div> <div ng-show=!ctrl.edit layout-gt-xs=row layout=column> <span flex></span> <md-button ng-if=\"app.user.current.id == ctrl.wallet.owner_id\" class=\"md-raised md-accent\" ng-click=ctrl.remove()> <span translate=\"\">Delete</span> </md-button> <md-button ng-if=\"app.user.current.id == ctrl.wallet.owner_id\" class=\"md-raised md-primary\" ng-click=\"ctrl.edit = true;\"> <span translate=\"\">Edit</span> </md-button> </div> </mb-titled-block>  <mb-titled-block ng-show=ctrl.edit mb-progress=ctrl.saving mb-title=\"{{'Edit wallet'| translate}}\" layout=column> <form name=myForm ng-action=ctrl.update() layout=column flex> <md-input-container> <label translate=\"\">Title</label> <input ng-model=ctrl.wallet.title name=title required> <div ng-messages=myForm.title.$error> <div ng-message=required><span translate=\"\">This field is required.</span></div> </div> </md-input-container> <md-input-container> <label translate=\"\">Currency</label> <input ng-model=ctrl.wallet.currency required> </md-input-container> <md-input-container> <label translate=\"\">Description</label> <input ng-model=ctrl.wallet.description> </md-input-container> <div layout=row> <span flex></span> <md-button class=\"md-raised md-accent\" ng-click=\"ctrl.edit = false\"> <span translate=\"\">Cancel</span> </md-button> <md-button class=\"md-raised md-primary\" ng-disabled=myForm.$invalid ng-click=ctrl.update()> <span translate=\"\">Save</span> </md-button> </div> </form> </mb-titled-block> <md-content> <md-tabs md-dynamic-height md-border-bottom>  <md-tab label=\"{{'Transfers'| translate}}\"> <md-content class=md-padding> <div ng-controller=\"AmdBankWalletTransfersCtrl as transferCtrl\" mb-preloading=\"transferCtrl.state === 'ideal'\" layout=column flex> <mb-pagination-bar mb-title=\"{{'Transfers'| translate}}\" mb-model=transferCtrl.queryParameter mb-sort-keys=transferCtrl.getProperties() mb-more-actions=transferCtrl.moreActions> </mb-pagination-bar> <md-progress-linear ng-if=\"transferCtrl.state === 'busy'\" style=\"margin: 0px; padding: 0px\" md-mode=indeterminate class=md-accent md-color> </md-progress-linear> <md-content mb-infinate-scroll=transferCtrl.loadNextPage() layout=column flex>  <md-list flex> <md-list-item ng-repeat=\"transfer in transferCtrl.items track by transfer.id\" ng-if=\"transfer.to_wallet_id === transferCtrl.wallet.id\" class=md-3-line> <wb-icon>control_point</wb-icon> <div class=md-list-item-text layout=column> <h3><span translate=\"\">Amount</span>: {{::transfer.amount|| 0}}</h3> <h4><span translate>From wallet</span>: {{::transfer.from_wallet_id}}</h4> <p>{{::transfer.description}}</p> </div> </md-list-item> <md-list-item ng-repeat=\"transfer in transferCtrl.items track by transfer.id\" ng-if=\"transfer.from_wallet_id === transferCtrl.wallet.id\" class=md-3-line> <wb-icon>remove_circle_outline</wb-icon> <div class=md-list-item-text layout=column> <h3><span translate=\"\">Amount</span>: {{::transfer.amount|| 0}}</h3> <h4><span translate>To wallet</span>: {{::transfer.to_wallet_id}}</h4> <p>{{::transfer.description}}</p> </div> </md-list-item> </md-list> </md-content> <div layout=column layout-align=\"center center\" ng-if=\"transferCtrl.state !== 'busy' && (!transferCtrl.items || transferCtrl.items.length === 0)\"> <p translate=\"\">No transfers found</p> </div> </div> </md-content> </md-tab>  <md-tab label=\"{{'Payments'| translate}}\"> <md-content class=md-padding> <div ng-controller=\"AmdBankWalletPaymentsCtrl as paymentCtrl\" mb-preloading=\"paymentCtrl.state === 'ideal'\" layout=column flex> <mb-pagination-bar mb-title=\"{{'Payments'| translate}}\" mb-model=paymentCtrl.queryParameter mb-sort-keys=paymentCtrl.getProperties() mb-more-actions=paymentCtrl.moreActions> </mb-pagination-bar> <md-progress-linear ng-if=\"paymentCtrl.state === 'busy'\" style=\"margin: 0px; padding: 0px\" md-mode=indeterminate class=md-accent md-color> </md-progress-linear> <md-content mb-infinate-scroll=paymentCtrl.loadNextPage() layout=column flex>  <md-list flex> <md-list-item ng-repeat=\"payment in paymentCtrl.items track by payment.id\" ng-href=/wallets/{{ctrl.wallet.id}}/payments/{{payment.id}} class=md-3-line> <wb-icon>control_point</wb-icon> <div class=md-list-item-text layout=column> <h3><span translate=\"\">Amount</span>: {{::payment.amount|| 0}}</h3>  <p>{{::payment.description}}</p> </div> </md-list-item> </md-list> </md-content> <div layout=column layout-align=\"center center\" ng-if=\"paymentCtrl.state !== 'busy' && (!paymentCtrl.items || paymentCtrl.items.length === 0)\"> <p translate=\"\">No payments found</p> </div> </div> </md-content> </md-tab> </md-tabs> </md-content> </md-content>"
  );


  $templateCache.put('views/amd-bank-wallets.html',
    "<div mb-preloading=ctrl.loading layout=column flex> <mb-pagination-bar mb-model=ctrl.queryParameter mb-reload=ctrl.reload() mb-sort-keys=ctrl.getSortKeys() mb-more-actions=ctrl.getActions()> </mb-pagination-bar> <md-content mb-preloading=\"ctrl.state==='busy'\" mb-infinate-scroll=ctrl.loadNextPage() layout=column flex>  <md-list flex> <md-list-item ng-repeat=\"wallet in ctrl.items track by wallet.id\" ng-if=!wallet.deleted ng-href=wallets/{{::wallet.id}} class=md-3-line> <wb-icon>account_balance_wallet</wb-icon> <div class=md-list-item-text layout=column> <h3>{{::wallet.title}}</h3> <h4><span translate=\"\">Balance</span>: {{wallet.total_deposit - wallet.total_withdraw || 0}} ({{::wallet.currency}})</h4> <p>{{::wallet.description}}</p> </div> <md-button ng-if=\"app.user.current.id == wallet.owner_id\" class=md-icon-button aria-label=\"Charge wallet\" ng-href=\"/wallets/charge?walletId={{::wallet.id}}\"> <wb-icon>battery_charging_80</wb-icon> <md-tooltip md-direction=bottom><span translate=\"\">Charge wallet</span></md-tooltip> </md-button> <md-button ng-if=\"app.user.current.id == wallet.owner_id\" class=md-icon-button aria-label=\"Wallet to wallet\" ng-href=\"/wallet-to-wallet?fromId={{::wallet.id}}\"> <wb-icon>swap_horiz</wb-icon> <md-tooltip md-direction=bottom><span translate=\"\">Wallet to wallet</span></md-tooltip> </md-button> <md-button ng-if=\"app.user.current.id == wallet.owner_id\" class=md-icon-button aria-label=\"Delete wallet\" ng-click=ctrl.deleteItem(wallet)> <wb-icon>delete</wb-icon> <md-tooltip><span translate=\"\">Delete wallet</span></md-tooltip> </md-button> </md-list-item> <div layout=column layout-align=\"center center\" ng-if=\"ctrl.state === 'ideal' && (!ctrl.items || ctrl.items.length === 0)\"> <h2 translate=\"\">Nothing found</h2> </div> </md-list> </md-content> </div>"
  );

}]);
