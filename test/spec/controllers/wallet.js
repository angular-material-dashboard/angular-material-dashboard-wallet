'use strict';

describe('Controller AmdWallet', function() {

	// load the controller's module
	beforeEach(module('ngMaterialDashboardWallet'));

	var AmdWalletCtrl;
	var scope;

	// Initialize the controller and a mock scope
	beforeEach(inject(function($controller, $rootScope, _$usr_) {
		scope = $rootScope.$new();
		AmdWalletCtrl = $controller('AmdBankWalletCtrl', {
			$scope : scope,
			$usr : _$usr_
			// place here mocked dependencies
		});
	}));

	it('should be defined', function() {
		expect(angular.isDefined(AmdWalletCtrl)).toBe(true);
	});
});
